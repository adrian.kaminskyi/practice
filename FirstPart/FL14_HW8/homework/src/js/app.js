let first = document.getElementById('first-ans');
let second = document.getElementById('second-ans');
let third = document.getElementById('third-ans');
let fourth = document.getElementById('fourth-ans');
let newGame = document.querySelector('#newGame');
let skip = document.querySelector('#skip');
let current = document.getElementById('current_prise');
let total_prise = document.getElementById('total_prise');
let question = document.getElementById('question');
let qBlock = new Object();
let correct;
let answers = document.getElementById('answers');
let questionIndex = 1;
let totalText = document.getElementById('totalText');
let currentText = document.getElementById('currentText');
let score = [
    '0',
    '100',
    '500',
    '1000',
    '5000',
    '10000',
    '50000',
    '100000',
    '500000',
    '1000000'
];
let ans = [
    first,
    second,
    third,
    fourth
];

function startNewGame(questions){
    answers.style.display = 'flex';
    question.style.display = 'block';
    current.innerHTML = score[0];
    total_prise.innerHTML = score[0];
    qBlock = randomQuestion(questions)();
    question.innerHTML = qBlock.question;
    first.innerText = qBlock.content[0];
    second.innerText = qBlock.content[1];
    third.innerText = qBlock.content[2];
    fourth.innerText = qBlock.content[3];
    correct = qBlock.correct;
    newGame.disabled = true;
    totalText.style.display = 'block';
    total_prise.style.display = 'block';
    currentText.style.display = 'block';
    current.style.display = 'block';
    currentText.innerText = 'Prise on current round: '; 
    totalText.innerText = 'Total prise: ';
    skip.style.display = 'block';
    skip.disabled = false;
}

function randomQuestion(InitialArray){
    let arr = new Array();
    function randomIndex(){
        return Math.floor(Math.random() * arr.length);
    }
    function reinitArray(){
        arr = InitialArray.slice();
    }
    reinitArray();
    return function getRandomElement(){
        if(arr.length === 0){
            reinitArray();
        }
        return arr.splice(randomIndex(),1)[0];
    }
}

function choosingAnswer(btnIndex, questions){
    if(btnIndex === correct){
        qBlock = randomQuestion(questions)();
        question.innerHTML = qBlock.question;
        first.innerText = qBlock.content[0];
        second.innerText = qBlock.content[1];
        third.innerText = qBlock.content[2];
        fourth.innerText = qBlock.content[3];
        correct = qBlock.correct;
        if(questionIndex < 10){
            current.innerHTML = score[questionIndex];
            questionIndex++;
        }else if(questionIndex === 10){
            answers.style.display = 'none';
            question.style.display = 'none';
            newGame.disabled = false;
            currentText.style.display = 'none';
            current.style.display = 'none';
            total_prise.style.display = 'none';
            totalText.innerText = 'Congratulations! You won 1000000.';
            skip.style.display = 'none';
        }
        
    }else{
        answers.style.display = 'none';
        question.style.display = 'none';
        newGame.disabled = false;
        totalText.style.display = 'none';
        total_prise.style.display = 'none';
        currentText.innerText = 'Game over! Your prize is: ';
        skip.style.display = 'none';
    }
    
}



function skipQuestion(questions){
    qBlock = randomQuestion(questions)();
    question.innerHTML = qBlock.question;
    first.innerText = qBlock.content[0];
    second.innerText = qBlock.content[1];
    third.innerText = qBlock.content[2];
    fourth.innerText = qBlock.content[3];
    skip.disabled = true;
}
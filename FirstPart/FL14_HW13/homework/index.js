function getAge(date) { 
    let today = new Date();
    let bthday = new Date(date);
    let age = today.getFullYear() - bthday.getFullYear();
    if(today.getMonth() > bthday.getMonth()) {
        return age;
    } else if(today.getMonth() === bthday.getMonth()){
        if(today.getDate() < bthday.getDate()) {
            return --age;
        } else {
            return age;
        }
    }
}

let arrOfWeekdays = [
    "Sunday", 
    "Monday", 
    "Tuesday", 
    "Wednesday", 
    "Thursday", 
    "Friday", 
    "Saturday"
];

function getWeekDay(date) {
    return arrOfWeekdays[date.getDay()];
  }
  
function getProgrammersDay(year) {
    let date = new Date(year,0, 256);
    return date.getDate() + ' Sep, ' + year + ` (${getWeekDay(date)})`; 
}

function howFarIs(specifiedWeekday) {
    let now = new Date();
    let today = getWeekDay(now);
    specifiedWeekday = specifiedWeekday.charAt(0).toUpperCase() + specifiedWeekday.slice(1);
    let number = 0;
    let todayNumberOfWeek = 0;
    let specifiedWeekdayNumOfWeek = 0;
        for (let i = 0; i < arrOfWeekdays.length; i++) {
            if (today === arrOfWeekdays[i]) {
                todayNumberOfWeek = i;
            } else {
                continue;
            }
        }

        for (let i = 0; i < arrOfWeekdays.length; i++) {
            if (specifiedWeekday === arrOfWeekdays[i]) {
                specifiedWeekdayNumOfWeek = i;
            } else {
                continue;
            }
        }
        if(todayNumberOfWeek === specifiedWeekdayNumOfWeek) {
            return `Hey, today is ${specifiedWeekday} =)`;
        } else if(todayNumberOfWeek > specifiedWeekdayNumOfWeek) {
            number = todayNumberOfWeek - specifiedWeekdayNumOfWeek;
            return `It's ${ number } day(s) left till ${ specifiedWeekday }.`;
        } else if(todayNumberOfWeek < specifiedWeekdayNumOfWeek){
            number = specifiedWeekdayNumOfWeek - todayNumberOfWeek;
            return `It's ${ number } day(s) left till ${ specifiedWeekday }.`;
        }
}

function isValidIdentifier (str) {
    const regExp = new RegExp(/^\d|[^a-zA-Z\d/$_]|^$|\s/gi).test(str);
    if(!regExp){
        return true
    } else {
        return false
    }
}

function capitalize (str) {
    return str.replace(/^(.)|\s+(.)/g, c => c.toUpperCase());
}

function isValidAudioFile (str) {
    return /^[A-Za-z]+\.(mp3|flac|alac|aac)$/.test(str);
}

function getHexadecimalColors(str) {
    let arr = []
    const reg = new RegExp(/#([a-f0-9]{3}){1,2}\b/gi);
    if(reg.test(str)){
        arr.push(str.match(reg));
        return arr;
    } else {
        return [];
    }
    
}

function isValidPassword (password) {
    return !!password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g)
}

function addThousandsSeparators(str) {
    return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


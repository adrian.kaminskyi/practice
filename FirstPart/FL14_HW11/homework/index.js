function isEquals (fst, sec) {
    return fst === sec;
}

function numberToString(num) {
    return String(num);
}

function storeNames() {
    let arr = [];
    for (let i = 0; i < arguments.length; i++){
       arr.push(arguments[i]);
    }
    return arr;
}

function isBigger(fst, sec) {
    if (fst > sec) {
        return fst/sec;
    } else if (fst < sec) {
        return sec/fst;
    } else {
        return 1;
    }
}

function getDivision (fst, sec) {
    if (isBigger(fst, sec) < 1 || isBigger(fst,sec) === 1){
       return 1;
    } else {
        return isBigger(fst, sec);
    }

}

function negativeCount (arr) {
    let count = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] < 0) {
            count++;
        }
    }
    return count;
}

function letterCount (str, letter) {
    let counter = 0;
    for(let i = 0; i < str.length; i++) {
        if (str.charAt(i) === letter) {
            counter++;
        }
    }
    return counter;
}

function countPoints(arr) {
    let counter = 0;
    for (i = 0; i < arr.length; i++) {
        let inter = arr[i].split(':', 2);
        let x = inter[0];
        let y = inter[1];
        if (x > y) {
            counter += 3; 
        } else if (x === y) {
            counter++;
        }
    }
    return counter;
}
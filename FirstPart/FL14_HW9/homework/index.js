const fruits = [
{name: 'apple', weight: 0.5 },
{name: 'pineapple', weight: 2 }
];

function convert() {
    let arrMap = [];
    for(let i = 0; i< arguments.length; i++){
        if(typeof arguments[i] === 'string'){
           arrMap[i] = Number(arguments[i]);
        }else if(typeof arguments[i] === 'number'){
            arrMap[i] = String(arguments[i]);   
        } 
    }
    return arrMap;
}

function executeforEach(arr, fcb){
     for(let i = 0;i<arr.length; i++) {
        arr[i] = fcb(arr[i]);
    }
}

function mapArray(arr, fcb){
    let arrMap = [];
    executeforEach(arr, function(i){
        if(typeof i === 'string'){
            i = +i;
            arrMap.push(fcb(i));  
        }else{
            arrMap.push(fcb(i));
        } 
        
    });
}

function filterArray(arr,fcb){
    let arrMap = [];
    executeforEach(arr, function(i){
        if(fcb(i)){
           arrMap.push(i); 
        }
        
    });
    return arrMap;
}

function getValuePosition(arr, pos){
    for(let i = 0; i < arr.length; i++){
      if(arr[i] === pos){
       return i+1;
      }
    }
    
}

function flipOver(str){
    let str2 = '';
    for(let i= str.length-1; i>=0; i--){
            str2 += str[i];
        }
    console.log(str2);
    
}

function makeListFromRange(arr){
    let start = arr[0];
    let end = arr[arr.length-1];
    let long = end - start;
    for(let i = 0; i <= long; i++){
        arr[i] = start;
        start++;
    }
}

function getArrayOfKeys(arr, prop){
    let arrMap = [];
    executeforEach(arr, function(i){
        arrMap.push(i[prop]);
    })
    return arrMap;
}



const basket = [
{ name: 'Bread', weight: 0.3 },
{ name: 'Coca-Cola', weight: 0.5 },
{ name: 'Watermelon', weight: 8 }
];

function getTotalWeight(arr){
    let total = 0;
    executeforEach(arr, function(i){
        total += i.weight;
    });
    console.log(total);
}

const date = new Date(2020, 0, 2);

function getPastDay(date, num){
    let anotherDay = new Date(date);
    anotherDay.setDate(date.getDate()- num);
    let dateNumber = anotherDay.getDate();
    console.log(dateNumber);
}

function formatDate(date){
    let year = '' + date.getFullYear();
    let month = '' + (date.getMonth() + 1); 
    if (month.length === 1) {
        month = '0' + month; 
    }
    let day = '' + date.getDate(); 
    if (day.length === 1) { 
        day = '0' + day; 
    }
    let hour = '' + date.getHours(); 
    if (hour.length === 1) { 
        hour = '0' + hour; 
    }
    let minute = '' + date.getMinutes();
    if (minute.length === 1) { 
        minute = '0' + minute; 
    }
    let second = '' + date.getSeconds();
    if (second.length === 1) {
        second = '0' + second;
    }
    return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
}

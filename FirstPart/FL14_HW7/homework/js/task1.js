let BreakException= {};
let battery
let defBattery
let amount
let rate
const allPercent = 100;
function f1() { 
        battery = +prompt('Input the amount of batteries', '')
    if(isNaN(battery)){
        alert('Invalid input data');
        throw BreakException;
    }else if(battery < 0){
        alert('Invalid input data');
        throw BreakException;
    }
        defBattery = +prompt('Input the amount of defective batteries', '')
    if(isNaN(defBattery)){
        alert('Invalid input data')
        throw BreakException;
    }else if(defBattery < 0){
        alert('Invalid input data');
        throw BreakException;
    }
    amount = battery + defBattery
    rate = battery / allPercent * defBattery
    alert('Amount of batteries: ' + amount + 
          '\nDefective rate: ' + Math.floor(rate)+ '%' +
          '\nAmount of defective batteries: ' + defBattery + 
          '\nAmount of working batteries: ' + battery)     
}

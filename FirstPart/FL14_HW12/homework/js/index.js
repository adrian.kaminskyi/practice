let storage = localStorage.getItem('path');
let page1Count = localStorage.getItem('page1');
let page2Count = localStorage.getItem('page2');
let page3Count = localStorage.getItem('page3');
function visitLink(path) {
	storage++;
	localStorage.setItem('path', storage);
	if (path === 'Page1'){
		page1Count++;
		localStorage.setItem('page1', page1Count);
	} else if (path === 'Page2') {
		page2Count++;
		localStorage.setItem('page2', page2Count);
	} else if (path === 'Page3') {
		page3Count++;
		localStorage.setItem('page3', page3Count);
	}
}


function viewResults() {
	let nodeUl = document.createElement('ul');
	let nodeLi1 = document.createElement('li');
	let page1 = document.createTextNode(`You visited Page 1 ${page1Count} time(s)`);
	let nodeLi2 = document.createElement('li');
	let page2 = document.createTextNode(`You visited Page 2 ${page2Count} time(s)`);
	let nodeLi3 = document.createElement('li');
	let page3 = document.createTextNode(`You visited Page 3 ${page3Count} time(s)`);
	nodeUl.appendChild(nodeLi1, nodeLi2, nodeLi3);
	nodeUl.appendChild(nodeLi2);
	nodeUl.appendChild(nodeLi3);
	nodeLi1.appendChild(page1);
	nodeLi2.appendChild(page2);
	nodeLi3.appendChild(page3);
	document.getElementById('content').appendChild(nodeUl);
	localStorage.clear();
}



let expression = '';
let result;
function writeMath () {
    expression = prompt('Write your mathematical expression');
}

function catchError () {
    try{
        writeMath();
        result = eval(expression);
        if (result === Infinity || result === undefined || result === NaN)  {
            throw Error;
        } else if (result === null) {
            throw Error;
        }
        alert(result);
    } catch(e) {
        alert('Wrong expresion. Write it again.');
        catchError();
         
    } 
        
}

catchError();
const data = [
  {
    folder: true,
    title: "Grow",
    children: [
      {
        title: "logo.png",
      },
      {
        folder: true,
        title: "English",
        children: [
          {
            title: "Present_Perfect.txt",
          },
        ],
      },
    ],
  },
  {
    folder: true,
    title: "Soft",
    children: [
      {
        folder: true,
        title: "NVIDIA",
        children: null,
      },
      {
        title: "nvm-setup.exe",
      },
      {
        title: "node.exe",
      },
    ],
  },
  {
    folder: true,
    title: "Doc",
    children: [
      {
        title: "project_info.txt",
      },
    ],
  },
  {
    title: "credentials.txt",
  },
];

function renderFileTree(node, parentNode) {
  let liNode = createLi(node);
  if (parentNode) {
    parentNode.appendChild(liNode);
  }
  if (node.folder && node.children != null) {
    node.children.forEach((element) => {
      renderFileTree(element, liNode);
      
    });
  }
  return liNode;
}

const rootNode = document.getElementById("root");
let rootUl = document.createElement("ul");
rootNode.appendChild(rootUl);

function createLi(node) {
  let li = document.createElement(node.folder ? "li" : "ul");
  let span = document.createElement("span");
  let text = document.createTextNode(node.title);
  let icon = document.createElement("i");
  icon.className = "material-icons";
  li.appendChild(icon);
  li.appendChild(span);
  span.appendChild(text);
  folder(node, icon);
  return li;
}



function folder(node, icon) {
  if (node.folder === true) {
    let iText = document.createTextNode("folder");
    icon.appendChild(iText);
    icon.setAttribute("style", "color:orange;");
  } else {
    let iText = document.createTextNode("insert_drive_file");
    icon.appendChild(iText);
    icon.setAttribute("style", "color: gray;");
  }
}

function main() {
  for (let i = 0; i < data.length; i++) {
    const rootElement = renderFileTree(data[i]);
    rootUl.appendChild(rootElement);
  }
  // contextMenu(rootNode);
}

main();
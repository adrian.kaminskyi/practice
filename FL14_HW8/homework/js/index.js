const $list = $(".list");
const $input = $("#add-input");
const $add = $("#add-submit");
const $form = $(".add-items");


const todos = [
  {
    text: "Buy milk",
    done: false
  },
  {
    text: "Play with dog",
    done: true
  }
];

$(document).ready(function() {
  $list.html(localStorage.getItem('listItems'));

  $form.submit(function (event)
  {
    event.preventDefault();

    let item = $input.val();

    if(item){
      $list.append(`<li class="item">
      <input class='checkbox' type='checkbox'/>${item}
      <button class="item-remove">Remove</button></li>`)

      localStorage.setItem('listItems',$list.html());

      $input.val('');
    }
  });

  $(document).on('change','.checkbox',function() {
    if($(this).attr('checked')) {
      i$(this).removeAttr('checked');
    } else {
      $(this).attr('checked', 'checked');
    }

    $(this).parent().toggleClass('completed');

    localStorage.setItem('listItems', $list.html());
  });

  $(document).on('click', '.item-remove', function() {

    $(this).parent().remove();

    localStorage.setItem('listItems', $list.html());
  })
});
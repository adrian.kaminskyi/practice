// function sendRequest ({method}, url) {
//    return fetch(url).then(response => {
//        return response.json()
//    })
// }
function render() {
    fetch('https://jsonplaceholder.typicode.com/users', {
        method: 'GET'  
    })
    .then((response) => response.json())
    .then(jsonData => {
        let tbody = document.getElementById('tbody');
        for (let i = 0; i < jsonData.length; i++) {
            let tr = '<tr>';
            tr += '<td>' + jsonData[i].id.toString() + '</td>' + 
                `<td onclick ='showPosts(${jsonData[i].id})'>` + jsonData[i].name + '</td>'+
                '<td>' + jsonData[i].username + '</td>'+
                '<td>' + jsonData[i].email + '</td>'+
                '<td><table><tr>' + '<td>' + jsonData[i].address.street + "</td>" +
                    '<td>' + jsonData[i].address.suite.toString() + "</td>" +
                    '<td>' + jsonData[i].address.city + "</td>" +
                    '<td>' + jsonData[i].address.zipcode.toString() + "</td>" +
                    '<td><table><tr>' + '<td>' + jsonData[i].address.geo.lat.toString() + "</td>" +
                        '<td>' + jsonData[i].address.geo.lng.toString() + "</td>" +
                    '</tr></table></td>'+
            '</tr></table></td>'+
            '<td>' + jsonData[i].phone.toString() + '</td>'+
            '<td>' + jsonData[i].website.toString() + '</td>'+
            '<td><table><tr>' + '<td>' + jsonData[i].company.name + "</td>" +
                '<td>' + jsonData[i].company.catchPhrase + "</td>" +
                '<td>' + jsonData[i].company.bs + "</td>" +
            '</tr></table></td>'+
            `<td><button class='btn-edit id-${jsonData[i].id}' onclick = 'edit(${jsonData[i].id})'>Edit</button>`+
            `<button class='btn-remove id-${jsonData[i].id}' onclick = 'removeUser(${jsonData[i].id})'>Delete</button>`+
            `<div class='loader id-${jsonData[i].id}'></div>`
            '</tr>';
            tbody.innerHTML += tr;
        }
        const cells = document.getElementsByTagName('td');
        const headerCells = document.getElementsByTagName('th');
    
        for (let cell of cells) {
        cell.style.border = '1px solid black';
        }
    
        for (let cell of headerCells) {
        cell.style.border = '1px solid black';
        }
        // let trEdit = document.getElementsByTagName('td');
        // for(let tr of trEdit){
        //     tr.contentEditable = "true";
        // }
    })
    .catch(err => console.log(err));
}



function edit(id) {
    document.querySelector(`.loader.id-${id}`).style.display = 'block';
    document.querySelector(`.btn-edit.id-${id}`).style.display = 'none';
    document.querySelector(`.btn-remove.id-${id}`).style.display = 'none';
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'applycation/json'
        },
    })
    .then((response) => {
        document.querySelector(`.loader.id-${id}`).style.display = 'none';
        document.querySelector(`.btn-edit.id-${id}`).style.display = 'block';
        document.querySelector(`.btn-remove.id-${id}`).style.display = 'block';
        return response.json();
    });
    
}

function removeUser(id) {
    document.querySelector(`.loader.id-${id}`).style.display = 'block';
    document.querySelector(`.btn-edit.id-${id}`).style.display = 'none';
    document.querySelector(`.btn-remove.id-${id}`).style.display = 'none';
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
        method: 'DELETE',
    })
    .then((response) => {
        document.querySelector(`.loader.id-${id}`).style.display = 'none';
        document.querySelector(`.btn-edit.id-${id}`).style.display = 'block';
        document.querySelector(`.btn-remove.id-${id}`).style.display = 'block';
        return response.json();
    });
}

render();

function showPosts(id) {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then(response => response.json())
    .then(json => {
            let posts = document.querySelector('#posts');
            posts.innerHTML+= '<tr><td>' + json.userId + '</td>'+
                '<td>' + json.id + '</td>'+
                '<td>' + json.title + '</td>'+
                '<td>' + json.body + '</td></tr>';
                const cells = document.getElementsByTagName('td');
                for (let cell of cells) {
                    cell.style.border = '1px solid black';
                    }
                
    })
    fetch(`https://jsonplaceholder.typicode.com/comments/${id}`)
    .then(response => response.json())
    .then(json => {
            let posts = document.querySelector('#posts');
            posts.innerHTML+= '<tr><td>' + json.postId + '</td>'+
                '<td>' + json.id + '</td>'+
                '<td>' + json.name + '</td>'+
                '<td>' + json.email + '</td>'+
                '<td>' + json.body + '</td></tr>';
                const cells = document.getElementsByTagName('td');
                for (let cell of cells) {
                    cell.style.border = '1px solid black';
                    }
                
    })
}
const maxElement =  arr => Math.max(...arr);

const copyArray = arr => [...arr];

const addUniqueId = obj => {
    const id = Symbol();
    const resObj = obj;
    resObj[id] = '1'
    return resObj
}

const regroupObject = obj =>  {
    const newObj = Object.create(obj);
    newObj.Unversity = obj.details.unversity;
    newObj.details = {};
    newObj.details.age = obj.details.age;
    newObj.details.firstName = obj.name;
    newObj.details.id = obj.details.id;
    return newObj;
}

const findUniqueElements = arr => {
    const onlyUnique = (value, index, self) => self.indexOf(value) === index; 
    return arr.filter(onlyUnique);
}

const hideNumber = str => {
    let starLength = str.length-4;
    console.log(starLength);
    return str = str.slice(starLength).padStart(starLength+3, '*');
}

const add = (er = Error, ...arg) => {
    let sum = er + arg.reduce((previous, current) => previous+current)
    return sum;
}

const userNames = () => {
    let arr = new Array();
    fetch('https://jsonplaceholder.typicode.com/users')
    .then((response) => response.json())
    .then((jsonData) => {
        for(let i = 0; i< jsonData.length; i++) {
            arr.push(jsonData[i].name)
        }
        arr.sort();
    })
    return arr;
}

async function userNamesAwait() {
    let arr = new Array();
    fetch('https://jsonplaceholder.typicode.com/users')
    .then((response) => response.json())
    .then((jsonData) => {
        for(let i = 0; i< jsonData.length; i++) {
            arr.push(jsonData[i].name)
        }
        arr.sort();
    })
    return arr;
}

console.log(userNamesAwait());